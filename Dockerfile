FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
MAINTAINER Daniel Juarez Gonzalez <djuarezg@cern.ch>

# Install latest release of cvmfs as per https://cernvm.cern.ch/fs/
RUN yum install -y https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm && yum install -y cvmfs cvmfs-config && yum clean all

# we need to create the cvmfs automount _inside_ a shared mount /cvmfsmounts so it can be shared with other containers
COPY auto.master /tmp/auto.master
RUN cat /tmp/auto.master >> /etc/auto.master

# config file imported from lxplus with some tweaks
COPY default.local /etc/cvmfs/default.local

# Add CMS specific config for CERN
COPY cms.cern.ch.local /etc/cvmfs/config.d/cms.cern.ch.local

#run automounter process in foreground
CMD /usr/sbin/automount -f --debug

